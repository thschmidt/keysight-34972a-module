# Keysight 34972A Module
Version 0.3.0

Module to control the `Keysight 34972A Acquisition Unit` with the [Temperature System Server](https://gitlab.mpcdf.mpg.de/thschmidt/temperature-system-server).

## Supported Connections
- USB
- LAN (Ethernet)

## Supported Cards
 - `34901A` (20 channels)
 - `34902A` (16 channels)

## Supported Sensors
 - `RTD`: Resistance Temperature Detector (Pt100, Pt1000)*
 - `FRTD`: Four-Wire Resistance Temperature Detector (Pt100, Pt1000)*
 - `TC`: Thermocouple (B, E, J, K, R, N, S, T)
 - `THER`: Thermistor (Resistance 2.25, 5, 10 kOhm)

*Alpha: 0.085

## Resources
 - [Product Page](https://www.keysight.com/us/en/product/34972A/lxi-data-acquisition-data-logger-switch-unit.html)