
###############################################################################
# IMPORTS
###############################################################################

# Time and clock related
import time
from datetime import datetime as dt

# Import pyvisa as communication driver
from pyvisa import ResourceManager


###############################################################################
# CLASS
###############################################################################

class K34972A():
    def __init__(self, module_basics):
        self._mob = module_basics
        
        # Variable to save connection link of device
        self.inst = ''
        
        # Next Ping varaiable:
        # everytime it reaches five it checks the connection to the device
        self.next_ping = 5
        
        # The day of the last readout -> Reset the loggers time at midnight
        self.last_day = dt.utcnow().strftime('%d')
        
        # Vendor Id and Product Id for usb connections    
        self.vid = '2391'
        self.pid = '8199'
        
    """------------------------- INITIAL FUNCTIONS -------------------------"""  

    def init_device(self, setup=False):
        """Initialize the device parameters"""
        if not setup:
            # Get the sensor information for the device
            self.get_sensors()
        
    """----------------------- CONNECTION FUNCTIONS -----------------------"""
    
    def open_connection(self, device_connection={}):
        """
        Open Connection to device . 
        'device_connection': the connection info
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        # pyvisa Resource Manager
        visa_version = self._mob.general_body['Connection']['VISA']
        if visa_version == 'pyvisa-py':
            self.rm = ResourceManager('@py')
        else:
            self.rm = ResourceManager()
        
        if device_connection['Type'] in ['Ethernet', 'USB']:
            if device_connection['Type'] == 'Ethernet':
                device_con = ('TCPIP0::' + device_connection['IP']
                              + '::inst0::INSTR')
            elif device_connection['Type'] == 'USB':
                serial_number = device_connection['SerialNumber']
                device_con = ('USB0::' + self.vid + '::' + self.pid
                              + '::' + serial_number + '::0::INSTR')
            # Check if device is available
            self.inst = self.rm.open_resource(device_con, 
                                              write_termination='\n', 
                                              read_termination='\n')
            # Send an ip ping to establish the connection
            self.inst.timeout = 2000
            self.check_connection()

        else:
            raise Exception("Only connections via ethernet or USB are supported")
            
    def close_connection(self, device_connection={}):
        """
        Close Connection to device. 
        'device_connection': the connection info:
            'TYPE': 'Ethernet/USB/Serial,
            'IP/SerialNumber/Port':'XX.XX.XX.XX/XXXXXX/COMX
        """
        pass
            
    def check_connection(self):
        """Check the connection to the device"""
        self.inst.query('*IDN?')
      
    """--------------------- SET AND REQUEST FUNCTIONS --------------------"""  
        
    def request_status(self, device_state, log_request):
        """
        Request the current status of the device (only during an active campaign)
        """
        self.readout_data(device_state, log_request)
    
    def set_parameter(self, channel, parameter, value):
        pass  
      
    """------------------------ CAMPAIGN FUNCTIONS ------------------------""" 
    
    def start_campaign(self):
        """Start a new campaign with init settings"""
        self.next_ping = 0
    
    def stop_campaign(self):
        """Stop current campaign"""
        self.reset_and_clear()
                
    def init_campaign(self, campaign_details):
        """Initialize all devices of a new campaign"""
        # Set interval of campaign

        # Create sensor dictionary and return used sensor types
        sens_types = self.get_sensor_types()
        self.dur = int(campaign_details['INTERVAL'])

        # Reset clock and clear cache of device
        self.reset_clock()
        self.reset_and_clear()
        
        # Configure device
        self.configure_setup(self.dur,
                             self._mob.campaign_sensors,
                             sens_types)
        
        time.sleep(0.1)
        error_message = ''
        while True:
            message = self.get_error()
            if '+0,"No error"' in message:
                break
            else:
                error_message += message + '\n'
            
        if error_message != '':
            raise Exception(error_message)
        self.next_ping = 0
       
 
    """------------------------- INITIAL FUNCTIONS -------------------------""" 
             
    def get_sensors(self):
        "Get allowed sensor and its types"
        if self.device_body['Sensors'] == None:
            self.device_body['Sensors'] = {}
        if self.device_body['GetSensors']:
            allowed_sensors = self.get_card()
            for sensor_type in self.device_body['SensorTypes']:
                sensors = ''
                pos = 1
                for num in allowed_sensors:
                    if num != 0:
                        if sensor_type != 'FRTD':
                            sensors += (self.create_sensor_string(num,
                                                                  pos) + '; ')
                        else:
                            sensors += (self.create_sensor_string(int(num/2),
                                                                  pos) + '; ')
                    pos += 1
                self.device_body['Sensors'][sensor_type] = sensors[:-2]
        
    def get_card(self):
        """Readout module card to get amount of sensors"""
        allowed_sensors_list = []
        for i in range(1,4):
            card_info = self.inst.query('SYST:CTYPe? ' + str(i) + '00')
            card = card_info.split(',')[1]
            if card == '34901A':
                allowed_sensors_list.append(20)
            elif card == '34902A':
                allowed_sensors_list.append(16)
            elif card == '0':
                allowed_sensors_list.append(0)
            else:
                raise Exception("""Card is unknown for temperature applications.
                                Please insert a different card or change
                                configs of this module""")
        return allowed_sensors_list
    
    def create_sensor_string(self, num, pos):
        """Convert sensor info into string"""
        lower_bound = pos*100 + 1
        upper_bound = pos*100 + num
        return f"{lower_bound}-{upper_bound}"
             
           
    """---------------------- LOGGER PREPARE FUNCTIONS ---------------------""" 
    
    def get_sensor_types(self):
        """
        Get all used sensors types for each devices
        from the campaign_sensors dictionary
        """
        device_sensors = self._mob.campaign_sensors
        sens_types = []
        for sensor in device_sensors:
            if device_sensors[sensor]['TYPE'] not in sens_types:
                sens_types.append(device_sensors[sensor]['TYPE'])
        return sens_types
            
    def configure_setup(self, dur, sens_dict, sens_types):
        """Configure the Keysight / Agilent Logger"""
        sens_list = ""

        for sens_type in sens_types:
            sensors = ""
            for sens in sens_dict:
                if sens_dict[sens]['TYPE'] == sens_type:
                    sensors += "," + sens
            s = sens_type.split("-")
            # Configure the sensor type of each channel
            if "THER" in sens_type:
                if str(s[1]) == "2.2":
                    s[1] = 2.252
                self.inst.write("CONF:TEMP " + s[0] + ","
                           + str(int(float(s[1])*1000)) + ", (@"
                           + sensors[1:] + ")") 
            else:    
                self.inst.write("CONF:TEMP " + s[0] + "," + s[1]
                           + ", (@" + sensors[1:] + ")") 
                if "RTD" in sens_type:
                    self.inst.write("SENS:TEMP:TRAN:" + s[0] + ":RES "
                               + str(s[2]) + ", " + "(@" + sensors[1:] + ")")
            sens_list += sensors

        self.write_config(sens_list[1:], dur)


    """------------------------- LOGGER FUNCTIONS -------------------------""" 

    def readout_logger(self):
        """Readout the device and fetch data in the memory"""
        sensors = len(self._mob.campaign_sensors)
        readings = int(self.inst.query("DATA:POIN?"))
        data = ""   
        # Check if a full dataset is available
        # (all sensors of the device have been readout)
        if readings >= sensors:  
            read_sensors = int(readings/sensors)*sensors
            data = self.inst.query("DATA:REM? " + str(read_sensors))
        return data
    
    # COMMAND FUNCTIONS
    def running(self, inst):
        """Check if device is still running by number of data in the memory"""
        return int(inst.query("DATA:POIN?")) > 0
          
    def request(self):
        """Start the device with 'device_id' """
        self.inst.write(":INIT")

    def get_error(self):
        """Get current System error of device with 'device_d' """
        return self.inst.query("SYST:ERR?")
        
    # INIT BLOCK  
    def reset_clock(self):
        """Reset the system clock of the device"""
        now_utc = dt.utcnow()
        dev_date = now_utc.strftime("%Y,%m,%d")
        dev_time = now_utc.strftime("%H,%M,%S.%f")[:-3]
        self.inst.write("SYST:TIME " + dev_time)
        self.inst.write("SYST:DATE " + dev_date)
        
    def reset_and_clear(self):
        """Reset and clear the device memory"""
        self.inst.write("*RST")
        self.inst.write("*CLS")
        
    def write_config(self, sens_list, dur):
        """Send temperature unit number of sensors etc. to device"""
        self.inst.write("UNIT:TEMP K, (@" + sens_list + ")")
        self.inst.write(":FORM:READ:TIME:TYPE ABS;"
                + ":FORM:READ:ALAR OFF;:FORM:READ:CHAN ON"
                + ";:FORM:READ:TIME ON;:FORM:READ:UNIT ON")
        
        self.initial_readout()
 
    def initial_readout(self):
        """Initial readout of sensor data to validate"""
        # Check sensor state
        self.request()
        i = 0
        data = ''
        while data == '':
            data = self.readout_logger()
            time.sleep(0.2)
            i += 1
            if i > 50:
                # Timeout
                raise Exception("Sensors could not be initialized")

    """------------------------- READOUT FUNCTIONS -------------------------""" 

        
    def readout_data(self, device_state, log_request):
        """
        Convert the readout data from the device
        and prepare it to send to the logger
        """

        if device_state == 'Busy':
            lsens, ldata = [],[]
            
            timestamp = self._mob.create_timestamp()
            
            if log_request:
                self.request()
                self.timestamp = timestamp
            
            raw_data = self.readout_logger()
            self.device_body['LastConnection'] = timestamp

            adata, lsens = self.arange_data(raw_data, lsens)
            if adata != []:
                if ldata == []:
                    ldata.append(adata[-1])
                else:
                    ldata[-1][1].update(adata[-1][1])

            if ldata != []:
                ddata = self.data_to_dict(ldata, self.timestamp)
                self.device_body['LastReadout'] = self.timestamp
                self.device_body['SensorData'] = ddata

            if dt.utcnow().strftime('%d') != self.last_day:
                self.reset_clock()

        elif device_state == 'Online':
            if self.next_ping == 5:
                self.check_connection()
          
        self.next_ping += 1
        if self.next_ping > 5:           
            self.next_ping = 0
            
        self.last_day = dt.utcnow().strftime('%d')
        
        
    """-------------------------- DATA FUNCTIONS --------------------------""" 
 
    def arange_data(self, readings, m_sens=[]):
        """Convert and arrange the rawdata directly read from the device"""
        data = []
        l_sens = []
        while readings != "":
            meas_time = ""
            meas_point_data = {}
            point_data = []
            if m_sens != []:
                sensors = m_sens
                m_sens = []
            else:
                sensors = self._mob.campaign_sensors.keys()
            for i in sensors:
                j = "," + str(i) + ","
                meas_point = readings[0:(readings.find(j))].split(',')
                if meas_point != [""]:
                    readings = readings[48:]
                    if meas_time == "":
                        meas_time = time_format(meas_point)
                        point_data.append(meas_time)
                    meas_point_data[str(i)] = temperature_format(meas_point)
                if str(i) not in meas_point_data:
                    l_sens.append(i)
            point_data.append(meas_point_data)    
            data.append(point_data)
        return data, l_sens
    
    def data_to_dict(self, data, timestamp):
        """Convert the string data into a dictionary"""
        dict_data = {}
        for i in data:
            dict_data[timestamp] = i[1]
            # ltimestamp = i[0]
        return dict_data
    

###############################################################################
# SUBFUNCTIONS - FORMATS
###############################################################################


def time_format(meas_point):
    """Converting the timedata of the logger"""
    year = meas_point[1]
    month = meas_point[2]
    day = meas_point[3]
    hour = meas_point[4]
    minute = meas_point[5]
    second = meas_point[6].split(".")[0]
    dateformat = (year + "-" + month + "-" + day + "T"
                  + hour + ":" + minute + ":" + second)
    return dateformat

def temperature_format(meas_point):
    """Convert temperature format and check for value above 1000 and below 0"""
    value = float(meas_point[0].replace("K",""))
    if value > 1000 or value < 0:
        value = "NaN"
    else:
        value = "{:.3f}".format(value)
    return value    