# Change Log for Keysight-34972a-Module
All notable changes to `Keysight-34972a-Module` will be documented in this file.

## 0.3.0 - 2024-11-07

### Added
 - Automatically fetch available sensor slots on connection
 - Selection of VISA backend
### Updated/Changed
 - Compatiblity with version 0.3 of Temperature System Server


## 0.2.0 - 2024-04-22

### Added
 - Support for usb connections (pyvisa)

### Updated/Changed
 - Switched to pyvisa
 - Module code structure changed: removed ask/send and initial parameters (now set by tempserver)


## 0.1.1 - 2024-03-19

### Added
 - Exception on currently non supported connections (Serial)

### Fixed
 - Fix reset of system clock of logger at midnight


## 0.1.0 - 2024-03-18
First Early Access Version released